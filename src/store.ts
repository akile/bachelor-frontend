import Vue from 'vue';
import Vuex, { mapActions } from 'vuex';
import axios from 'axios';
import toastr from 'toastr';

Vue.use(Vuex);


const CREATE_NEW_USER = 'CREATE_NEW_USER';
const UPDATE_USER = 'UPDATE_USER';
const DELETE_USER = 'DELETE_USER';
const GET_USER = 'GET_USER';
const GET_USERS = 'GET_USERS';


// OPTION 1
const host = process.env.VUE_APP_BACKEND_HOST;
const port = process.env.VUE_APP_BACKEND_PORT;
const API_URL_1 = `http://${host || 'localhost'}:${port || 8000}`; // API_URL to server

// OPTION 2
const API_URL_2 = `http://${process.env.BACKEND_PHP_SERVICE_HOST || 'localhost'}:${process.env.BACKEND_PHP_SERVICE_PORT || '8000'}`

const API_URL = API_URL_2;

interface IUser {
  ID: string;
  USERNAME: string;
  EMAIL: string;
}

export default new Vuex.Store({
  state: {
      user: {},
      users: [] as IUser[],
    },
  mutations: {
    [CREATE_NEW_USER](state, user: IUser) {
      state.users.unshift(user);
    },

    [GET_USERS](state, users: IUser[]) {
      state.users = users;
    },

    [GET_USER](state, user: IUser) {
      state.user = user;
    },

    [DELETE_USER](state, userId: string) {
      for (let i = 0; i < state.users.length; i++) {
        if (state.users[i].ID == userId) {
          state.users.splice(i, 1);
        }
      }
    },

    [UPDATE_USER](state, user: IUser) {
      for (const u of state.users) {
        if (u.ID == user.ID) {
          if (user.USERNAME) {
            Vue.set(u, 'USERNAME', user.USERNAME.trim());
          }
          if (user.EMAIL) {
            Vue.set(u, 'EMAIL', user.EMAIL.trim());
          }
        }
      }
    },
  },

  actions: {
    createNewUser({commit}, newUser: IUser) {
      console.log('API_URL_1: ' + API_URL_1);
      console.log('API_URL_2: ' + API_URL_2);
    
      axios.post(`${API_URL}/users`,
      {
        USERNAME: newUser.USERNAME,
        EMAIL: newUser.EMAIL,
      })
      .then((response) => {
        newUser.ID = response.data;
        commit(CREATE_NEW_USER, newUser);
        toastr.success('User successfully added!', 'SUCCESS');
        return response;
      }, (reject) => {
        toastr.error('User could not be created: ' + reject, 'ERROR');
      })
      .catch((error) => {
        error.json().then((resp: any) => {
          toastr.error(resp, 'ERROR');
          return Promise.reject(new Error('Failed to create new user: ' + error));
        });
      });
    },

    getUsers({commit}) {
      axios.get(`${API_URL}/users`)
      .then((response) => {
        commit(GET_USERS, response.data);
        return response;
      })
      .catch((error) => {
        toastr.error(error, 'ERROR');
        return Promise.reject(new Error('Failed to fetch users... ' + error));
      });
    },

    getUser({commit}, userId: number) {
      axios.get(`${API_URL}/users/${userId}`)
      .then((response) => {
        commit(GET_USER, response.data);
        return response;
      })
      .catch((error) => {
        toastr.error(error, 'ERROR');
        return Promise.reject(new Error('Failed to fetch user... ' + error));
      });
    },

    deleteUser({commit}, userId) {
      axios.delete(`${API_URL}/users/${userId}`)
      .then((response) => {
        commit(DELETE_USER, userId);
        toastr.success('User successfully deleted', 'SUCCESS');
        return response;
      })
      .catch((error) => {
        toastr.error(error, 'ERROR');
        return Promise.reject(new Error('Failed to delete user: ' + error));
      });
    },

    updateUser({commit}, updatedUser: IUser) {
      axios.put(`${API_URL}/users/${updatedUser.ID}`, {
        ID: updatedUser.ID,
        USERNAME: updatedUser.USERNAME,
        EMAIL: updatedUser.EMAIL,
      })
      .then((response) => {
        commit(UPDATE_USER, updatedUser);
        toastr.success('User successfully updated', 'SUCCESS');
        return response;
      })
      .catch((error) => {
        toastr.error(error, 'ERROR');
        return Promise.reject(new Error('Failed to update user: ' + error));
      });
    },
  },
});
