import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/createUser',
      name: 'createuser',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "createuser" */ './components/CreateUser.vue'),
    },
    {
      path: '/deleteUser',
      name: 'deleteuser',
      component: () => import(/* webpackChunkName: "deleteuser" */ './components/DeleteUser.vue'),
    },
    {
      path: '/updateUser',
      name: 'updateuser',
      component: () => import(/* webpackChunkName: "updateuser" */ './components/UpdateUser.vue'),
    },
    {
      path: '/getUser/',
      name: 'getuser',
      component: () => import(/* webpackChunkName: "getuser" */ './components/GetUser.vue'),
    },
    {
      path: '/getUser/:id',
      name: 'getuserbyid',
      component: () => import(/* webpackChunkName: "getuser" */ './components/GetUser.vue'),
    },
  ],
});
