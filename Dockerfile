FROM node:lts-alpine

# RUN npm install -g http-server

RUN mkdir /opt/frontend
RUN cd /opt/frontend

COPY *.json /opt/frontend/

WORKDIR /opt/frontend

RUN npm install -y

COPY . /opt/frontend

EXPOSE 8080/tcp

# USED FOR OPTION 1 in store.ts
# RUN chmod 666 /opt/frontend/.env
# RUN chmod +x /opt/frontend/addService.sh
# ENTRYPOINT ["/opt/frontend/addService.sh"]

CMD ["npm","run" ,"serve"]