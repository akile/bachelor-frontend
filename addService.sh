#!/bin/sh
set -e

variables="\nVUE_APP_BACKEND_HOST=$BACKEND_NODEJS_SERVICE_HOST\nVUE_APP_BACKEND_PORT=$BACKEND_NODEJS_SERVICE_PORT"
echo -e "$variables" >> .env

exec "$@"